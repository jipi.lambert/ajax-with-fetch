const { src, dest, watch } = require('gulp');
const concat = require('gulp-concat');

function compile_js(){
    return src(['scripts/*.js'])
        .pipe(concat('bundle.js'))
        .pipe(dest('bundle/'));
}

exports.default = function(){
    watch("./scripts/*.js", compile_js);
}