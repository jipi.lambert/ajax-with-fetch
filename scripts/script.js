const app = document.getElementById('app');
const form_data = document.getElementById('add');
const url =  app.dataset.ajaxUrl;

function getData(){
    fetch(url)
        .then(response => response.json())
        .then(json => processNotes(json))
        .catch(err => console.log('Requete erronnée ', err));
}

function deleteData(id){
    fetch(url + id, {
        method:"DELETE"
    })
        .then(response => response.text())
        .then(text => {
            updateUi(text);
        })
        .catch(err => console.log('Requete erronnée ', err));
}

function saveData(data){
    fetch(url, {
        method: "POST",
        body: JSON.stringify(data),
        headers: { "Content-type": "application/json; charset=UTF-8" }
    })
        .then(response => response.text())
        .then(text => {
            updateUi(text);
        })
        .catch(err => console.log('Requete erronnée ', err));
}

function updateUi( text){
    app.innerText = "";
    console.log(text);
    getData();
}

let processNotes = (notes) => {
    for (let i = 0; i < notes.length; i++) {
        let wrapper = document.createElement('div');
        let del = document.createElement('p');
        let p = document.createElement('p');
        wrapper.classList.add("note");
        p.append(notes[i].note);
        del.append("Delete note :" + notes[i].title);
        del.classList.add("delete");
        del.dataset.noteId = notes[i]._id;
        wrapper.append(p);
        wrapper.append(del);
        del.addEventListener("click", (e) =>{
            let note_id = e.target.dataset.noteId;
            deleteData(note_id);
        })
        app.append(wrapper);
    }
}

form_data.addEventListener("submit", (e) => {
    e.preventDefault();
    let formdata = new FormData(e.target);
    let dataset = {};
    for (let data of formdata.entries()) {
        dataset[data[0]] = data[1];
    }
    saveData(dataset);
});

getData();